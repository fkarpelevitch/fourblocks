package com.icdesignart.fourblocks;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.SurfaceTexture;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.TextureView;
import android.view.View;
import android.widget.TextView;

import com.google.common.collect.Iterables;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import java.util.TreeSet;


public class MainActivity extends ActionBarActivity implements TextureView.SurfaceTextureListener {

    private static final int[][] PIECES = new int[][]{
            {-1,0, 1,0, 2, 0},
            {1 ,0, 1,1, 0, 1},
            {-1,0, 1,0, 0, 1},
            {-1,0, 0,1, 1, 1},
            {1 ,0, 0,1,-1, 1},
            {1 ,0,-1,0,-1, 1},
            {1 ,0,-1,0, 1, 1},
    };
    public static final String STATIC_BLOCKS = "staticBlocks";
    public static final String CURRENT_PIECE = "currentPiece";
    public static final String TOP = "top";
    public static final String CENTER_X = "centerX";
    public static final String CENTER_Y = "centerY";
    public static final String ROW_FILL = "rowFill";
    public static final String STATE = "state";
    public static final String SCORE = "score";
    public static final String LINES = "lines";
    public static final String NEXT_DROP = "nextDrop";
    private TextureView tetrisView;
    private RenderingThread mThread;
    private Tetris tetris;
    private View mainView;
    private TextView scoreValue;
    private TextView linesValue;
    private TextView statusText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        tetris = new Tetris();
        mainView = getLayoutInflater().inflate(R.layout.activity_main, null);
//        getWindow().setFlags(FLAG_FULLSCREEN | FLAG_LAYOUT_IN_SCREEN,
//                FLAG_FULLSCREEN | FLAG_LAYOUT_IN_SCREEN);
        tetrisView = (TextureView) mainView.findViewById(R.id.tetris_view);
        tetrisView.setSurfaceTextureListener(this);
        tetrisView.setMinimumWidth(tetrisView.getWidth() / tetris.getWidth() * tetris.getWidth());
        scoreValue = (TextView) mainView.findViewById(R.id.score_value);
        linesValue = (TextView) mainView.findViewById(R.id.lines_value);
        statusText = (TextView) mainView.findViewById(R.id.status_text);

        setContentView(mainView);

        if (savedInstanceState!=null) {
            restoreState(savedInstanceState);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        synchronized (tetris) {
            outState.putIntArray(STATIC_BLOCKS, flatten(tetris.staticBlocks));
            outState.putIntArray(CURRENT_PIECE, flatten(tetris.currentPiece));
            outState.putIntArray(TOP, tetris.top);
            outState.putInt(CENTER_X, tetris.centerX);
            outState.putInt(CENTER_Y, tetris.centerY);
            outState.putIntArray(ROW_FILL, tetris.rowFill);
            outState.putInt(STATE, tetris.state);
            outState.putInt(SCORE, tetris.score);
            outState.putInt(LINES, tetris.lines);
            outState.putLong(NEXT_DROP, tetris.nextDrop);
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        restoreState(savedInstanceState);
    }

    private void restoreState(Bundle savedInstanceState) {
        synchronized (tetris) {
            unflatten(tetris.staticBlocks, savedInstanceState.getIntArray(STATIC_BLOCKS));
            unflatten(tetris.currentPiece, savedInstanceState.getIntArray(CURRENT_PIECE));
            arraycopy(tetris.top, savedInstanceState.getIntArray(TOP));
            tetris.centerX = savedInstanceState.getInt(CENTER_X);
            tetris.centerY = savedInstanceState.getInt(CENTER_Y);
            arraycopy(tetris.rowFill, savedInstanceState.getIntArray(ROW_FILL));
            tetris.state = savedInstanceState.getInt(STATE);
            tetris.score = savedInstanceState.getInt(SCORE);
            tetris.lines = savedInstanceState.getInt(LINES);
            tetris.nextDrop = savedInstanceState.getLong(NEXT_DROP);
            tetris.updateStatus();
            tetris.updateScore();
        }
    }

    private void arraycopy(int[] target, int[] src) {
        System.arraycopy(src, 0, target, 0, target.length);
    }

    private void unflatten(ArrayList<int[]> target, int[] intArray) {
        target.clear();
        for (int i = 0; i < intArray.length; i+=3) {
            target.add(new int[]{intArray[i], intArray[i+1], intArray[i+2]});
        }
    }

    private static int[] flatten(ArrayList<int[]> blocks) {
        int[] staticBlockArray = new int[blocks.size()*3];
        int k = 0;
        for (int i = 0; i < blocks.size(); i++) {
            for (int anInt : blocks.get(i)) {
                staticBlockArray[k++] = anInt;
            }
        }
        return staticBlockArray;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        synchronized (tetris) {
            if (tetris.state == tetris.RUNNING) {
                tetris.toggleState();
            }
        }
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        mThread = new RenderingThread(tetrisView);
        mThread.start();
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
    }

    

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        if (mThread != null) mThread.stopRendering();
        return true;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {

    }

    public void moveLeft(View view) {
        tetris.moveLeft();
    }

    public void moveRight(View view) {
        tetris.moveRight();
    }

    public void rotate(View view) {
        tetris.rotate();
    }

    public void toggleState(View view) {
        tetris.toggleState();
    }

    public void drop(View view) {
        tetris.drop();
    }

    class RenderingThread extends Thread {

        private final TextureView tetrisView;
        private volatile boolean running = true;

        public RenderingThread(TextureView tetrisView) {
            this.tetrisView = tetrisView;
        }


        @Override
        public void run() {
            int[] colors = new int[]{Color.BLUE, Color.GREEN, Color.RED, Color.WHITE, Color.YELLOW, Color.MAGENTA, Color.CYAN};
            Paint[] paints = new Paint[colors.length];
            for (int i = 0; i < colors.length; i++) {
                paints[i] = new Paint();
                paints[i].setColor(colors[i]);
//                paints[i].setShader(new LinearGradient(0, 1, 0, 50, colors[i], colors[(i + 1) % colors.length], Shader.TileMode.MIRROR));
            }
            while (running && !interrupted()) {

                Canvas canvas = tetrisView.lockCanvas();
                try {
                    canvas.drawColor(Color.BLACK);
                    int cellWidth = canvas.getWidth() / tetris.getWidth();
                    int cellHeight = canvas.getHeight() / tetris.getHeight();
                    int offsetX = (canvas.getWidth() - cellWidth * tetris.getWidth())/2;
                    int offsetY = canvas.getHeight() - cellHeight * tetris.getHeight();
                    synchronized (tetris) {
                        Iterable<int[]> blocks = tetris.getState();
                        for (int[] block : blocks) {
                            int x = block[0];
                            int y = block[1];
                            int paintIndex = block[2];
                            int left = offsetX + cellWidth * x;
                            int top = offsetY + cellHeight * y;
                            canvas.drawRect(left, top, left + cellWidth - 1, top + cellHeight - 1, paints[paintIndex]);
                        }
                    }
                    if (tetris.state != Tetris.RUNNING) {
                        Bitmap bitmap = ((BitmapDrawable) getResources().getDrawable(R.drawable.play)).getBitmap();
                        canvas.drawBitmap(bitmap, canvas.getWidth() / 2 - bitmap.getWidth()/2, canvas.getHeight() / 2 - bitmap.getHeight()/2, paints[0]);
                    }
                } finally {
                    tetrisView.unlockCanvasAndPost(canvas);
                }
                try {
                    synchronized (tetris) {
                        long sleepTime = tetris.updateState();
                        tetris.wait(sleepTime);
                    }
                } catch (InterruptedException e) {
                }
            }
        }

        public void stopRendering() {
            running = false;
            interrupt();
        }
    }


    private class Tetris {
        private static final int PAUSED = 1;
        private static final int NOT_RUNNING = 0;
        private static final int RUNNING = 2;
        private static final int GAME_OVER = 3;
        private static final int width = 17;
        private static final int height = 20;
        private final Random random = new Random(System.currentTimeMillis());
        private final ArrayList<int[]> staticBlocks = new ArrayList<>();
        private final ArrayList<int[]> currentPiece = new ArrayList<>(4);
        private final int[] top;
        private long nextDrop = System.currentTimeMillis();
        private int centerX,centerY;
        private int[] rowFill = new int[height];
        private int state = NOT_RUNNING;
        private int score = 0;
        private int lines = 0;

        private Tetris() {
            top = new int[width];
            for (int i = 0; i < top.length; i++) {
                top[i] = height;
            }
        }


        public int getWidth() {
            return width;
        }

        public int getHeight() {
            return height;
        }

        public synchronized Iterable<int[]> getState() {
            return Iterables.concat(staticBlocks, currentPiece);
        }

        public synchronized long updateState() {
            while (state == RUNNING && nextDrop <= System.currentTimeMillis()) {
                drop1();
                notifyAll();
            }
            return state==RUNNING?Math.max(16L, nextDrop - System.currentTimeMillis()):500L;
        }

        private boolean drop1() {
            boolean canMove = true;
            for (int[] point : currentPiece) {
                int newY = point[1] + 1;
                if (newY == height) {
                    canMove = false;
                    break;
                }
                int diff = top[point[0]] - newY;
                if (diff == 0) {
                    canMove = false;
                    break;
                } else if (diff < 0) {
                    for (int[] staticBlock : staticBlocks) {
                        if (staticBlock[0] == point[0] && staticBlock[1] == newY) {
                            canMove = false;
                            break;
                        }
                    }
                }
            }
            if (canMove) {
                for (int[] point : currentPiece) {
                    point[1]++;
                }
                centerY++;
            } else {
                TreeSet<Integer> rowsToRemove = null;
                for (int[] piece : currentPiece) {
                    if (top[piece[0]] > piece[1]) {
                        top[piece[0]] = piece[1];
                    }
                    if(++rowFill[piece[1]]==width) {
                        if(rowsToRemove==null) {
                            rowsToRemove = new TreeSet<>();
                        }
                        rowsToRemove.add(piece[1]);
                    }
                    staticBlocks.add(piece);
                    score+=3;
                }
                if (rowsToRemove!=null) {
                    score+=100 * rowsToRemove.size() * rowsToRemove.size();
                    lines+=rowsToRemove.size();
                    int i = height - 1;
                    int curshift = 0;
                    int[] shift = new int[height];

                    for (Integer rowToRemove : rowsToRemove.descendingSet()) {
                        while (i>rowToRemove) {
                            shift[i--] = curshift;
                        }
                        shift[i--] = -1;
                        curshift++;
                    }
                    while (i>=0) {
                        shift[i--] = curshift;
                    }
                    for (int j = 0; j < top.length; j++) {
                        top[j]=height;
                    }
                    Iterator<int[]> blocks = staticBlocks.iterator();
                    while (blocks.hasNext()) {
                        int[] block = blocks.next();
                        int shiftBy = shift[block[1]];
                        if (shiftBy == -1) {
                            blocks.remove();
                        } else {
                            block[1]+=shiftBy;
                            if (top[block[0]] > block[1]) {
                                top[block[0]] = block[1];
                            }
                        }
                    }
                    for (int j = height - 1; j >=0 ; j--) {
                        if (shift[j] > 0)
                        rowFill[j+shift[j]] = rowFill[j];
                        if (j<curshift) {
                            rowFill[j] = 0;
                        }
                    }
                }
                currentPiece.clear();
            }
            updateScore();
            if(currentPiece.isEmpty()) {
                int i = random.nextInt(PIECES.length);
                int[] piece = PIECES[i];
                centerX = width/2;
                centerY = 0;
                currentPiece.add(new int[]{centerX,centerY,i});
                for (int j = 0; j < piece.length; j+=2) {
                    int x = centerX + piece[j];
                    int y = centerY + piece[j + 1];
                    currentPiece.add(new int[]{x, y,i});
                    if (top[x]==y) {
                        state = GAME_OVER;
                        updateStatus();
                    }
                }
                nextDrop = System.currentTimeMillis() + 1000L;
            } else {
                nextDrop += 100L + (1200L / (2+lines/5)); // gradually decreases from 700ms to 100ms
            }
            return canMove;
        }

        public synchronized void moveLeft() {
            if (state != RUNNING) return;
            for (int[] piece : currentPiece) {
                if (piece[0] == 0) return;
                int newX = piece[0] - 1;
                int topDiff = piece[1] - top[newX];
                if (topDiff == 0) return;
                if (topDiff > 0) {
                    for (int[] block : staticBlocks) {
                        if (block[0] == newX && block[1] == piece[1]) return;
                    }
                }
            }
            for (int[] piece : currentPiece) {
                piece[0]--;
            }
            centerX--;
            notifyAll();
        }

        public synchronized void moveRight() {
            if (state != RUNNING) return;
            for (int[] piece : currentPiece) {
                int newX = piece[0] + 1;
                if (newX == width) return;
                int topDiff = piece[1] - top[newX];
                if (topDiff == 0) return;
                if (topDiff > 0) {
                    for (int[] block : staticBlocks) {
                        if (block[0] == newX && block[1] == piece[1]) return;
                    }
                }
            }
            for (int[] piece : currentPiece) {
                piece[0]++;
            }
            centerX++;
            notifyAll();
        }

        public synchronized void rotate() {
            if (state != RUNNING) return;
            for (int[] piece : currentPiece) {
                int newX = centerX + centerY - piece[1];
                if (newX >= width || newX<0) return;
                int newY = centerY + piece[0] - centerX;
                if (newY<0 || newY==top[newX]) return;
                if (newY>top[newX]) {
                    for (int[] block : staticBlocks) {
                        if (block[0] == newX && block[1] == newY) return;
                    }
                }
            }
            for (int[] piece : currentPiece) {
                int newX = centerX + centerY - piece[1];
                int newY = centerY + piece[0] - centerX;
                piece[0] = newX;
                piece[1] = newY;
            }
            notifyAll();
        }

        public synchronized void toggleState() {
            switch (state) {
                case RUNNING:
                    state = PAUSED;
                    nextDrop -= System.currentTimeMillis();
                    break;
                case PAUSED:
                    state = RUNNING;
                    nextDrop += System.currentTimeMillis();
                    break;
                case NOT_RUNNING:
                case GAME_OVER:
                    state = RUNNING;
                    reset();
                    break;
                default:
                    throw new IllegalStateException("Unknown state: " + state);
            }

            updateStatus();

            notifyAll();
        }

        private void updateStatus() {
            statusText.post(new Runnable() {
                @Override
                public void run() {
                    switch (state) {
                        case PAUSED:
                            statusText.setText(R.string.resume);
                            break;
                        case NOT_RUNNING:
                            statusText.setText(R.string.start_new_game);
                            break;
                        case GAME_OVER:
                            statusText.setText(R.string.game_over);
                            break;
                    }
                    statusText.setVisibility(state==RUNNING? View.INVISIBLE:View.VISIBLE);
                }
            });
        }

        private void reset() {
            staticBlocks.clear();
            currentPiece.clear();
            for (int i = 0; i < width; i++) {
                top[i] = height;
            }
            for (int i = 0; i < height; i++) {
                rowFill[i] = 0;
            }
            nextDrop = System.currentTimeMillis();
            score = 0;
            lines = 0;
        }

        public synchronized void drop() {
            if (state != RUNNING) return;
            while(drop1()) {
                score++;
            }
        }

        private synchronized void updateScore() {
            scoreValue.post(new Runnable() {
                @Override
                public void run() {
                    scoreValue.setText("" + tetris.score);
                    linesValue.setText("" + lines);
                }
            });

        }
    }
}
